### What are Glass Loupes? ###

The Glass Loupes repository contains 123dx and STL format models of clips used to attach Google Glass to surgical loupes. Currently, we support only the Yeoman loupes.

### Included ###

* Yeoman two-part clips, one for the front and one for the back.
* Yeoman single "all-in-one" clip for attaching Glass securely. This the preferred clip.

### Citation ###

Publications or other activities making substantial use of Glass Loupes or their derivatives should cite this this package and freely available online repository. An example citation is given as:

    W. Bartlett, A. B. Costa, "Glass Loupes", Package Version <Insert Version Number>,
    http://bitbucket.org/sinaineurosurgery/glassloupes

### Contact ###

Anthony B. Costa, anthony.costa@mssm.edu
